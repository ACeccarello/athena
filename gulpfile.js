/*!
 * gulp
 * $ npm install gulp-phpunit gulp-ruby-sass gulp-autoprefixer gulp-minify-css gulp-jshint gulp-concat gulp-uglify gulp-imagemin gulp-notify gulp-rename gulp-livereload gulp-cache main-bower-files del --save-dev
 */

// Load plugins
var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    livereload = require('gulp-livereload'),
    del = require('del'),
    phpunit = require('gulp-phpunit'),
    mainBowerFiles = require('main-bower-files');


// Main Bower Files
gulp.task('bower', function() {
    return gulp.src(mainBowerFiles(/* options */), { base: 'app/assets/vendor' })
        .pipe(gulp.dest('public/vendor'))
});


// tests
gulp.task('phpunit', function() {
    var options = {debug: true, notify: true};
    gulp.src('app/tests/*.php')
        .pipe(phpunit('./vendor/bin/phpunit', options))
        .on('error', notify.onError({
            title: 'PHPUnit Failed',
            message: 'One or more tests failed, see the cli for details.',
            icon:    __dirname + '/node_modules/gulp-phpunit/assets/test-fail.png'
        }))

        //will fire only if no error is caught
        .pipe(notify({
            title: 'PHPUnit Passed',
            message: 'All tests passed!',
            icon:    __dirname + '/node_modules/gulp-phpunit/assets/test-pass.png'
        }));
});



// Styles
gulp.task('styles', function() {
  return gulp.src('app/assets/sass/main.scss')
    .pipe(sass({ style: 'expanded', "sourcemap=none": true }))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulp.dest('public/styles'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(minifycss())
    .pipe(gulp.dest('public/styles'))
    .pipe(notify({ message: 'Styles task complete' }));
});

// Scripts
gulp.task('scripts', function() {
  return gulp.src('app/assets/scripts/**/*.js')
    .pipe(jshint('.jshintrc'))
    .pipe(jshint.reporter('default'))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('public/scripts'))
    .pipe(rename({ suffix: '.min' }))
    .pipe(uglify())
    .pipe(gulp.dest('public/scripts'))
    .pipe(notify({ message: 'Scripts task complete' }));
});

// Images
gulp.task('images', function() {
  return gulp.src('public/images/**/*')
    .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })))
    .pipe(gulp.dest('public/images'))
    .pipe(notify({ message: 'Images task complete' }));
});

// Clean
gulp.task('clean', function(cb) {
    del(['dist/assets/css', 'dist/assets/js', 'dist/assets/img'], cb)
});

// Default task
gulp.task('default', ['clean'], function() {
    gulp.start('styles', 'scripts', 'images');
});

// Watch Tests
gulp.task('test', function() {

  gulp.watch('app/**/*.php', ['phpunit']);

})

// Watch
gulp.task('watch', function() {

  // Watch .scss files
  gulp.watch('app/assets/sass/**/*.scss', ['styles']);

  // Watch .js files
  gulp.watch('app/assets/js/**/*.js', ['scripts']);

  // Watch image files
  gulp.watch('public/images/**/*', ['images']);

  // Watch bower installs
  gulp.watch('app/assets/vendor/**/*', ['bower']);

  // Create LiveReload server
  livereload.listen();

  // Watch any files in app/, reload on change
  gulp.watch(['app/views/**/*','app/controllers/*','app/routes.php','app/assets/**/*']).on('change', livereload.changed);

});