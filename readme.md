#Athena Project

Mooc para las universidades de latinoamerica

**Hecho Por Alessandro Ceccarello**

## Falta Por hacer:
* [Cursos](#cursos)
    + [Modulos](#modulos)
* [Usuarios](#usuarios)
* [Universidades](#universidades)
* [Cómo Funciona](#como-funciona)

### Cursos:
En el index de cursos:
* Acomodar bien el sidebar con todos los filtros necesarios
* Agregar fecha de inicio a los cursos y universidad.

En La Seccion de Curso individual:
* Mejorar un poco mas los estilos

Sección de crear curso (para profesores+)

Seccion de ver cursos en espera para aprobacion (managers, admins)


### Usuarios:
Por default el 1er user es Admin.

Es necesario hacer el modulo de crear usuario, editar usuario (diferente para cada rol), Eliminar usurario (logical), Ver Actividad de usuario (Admin only, probably next release).

Los roles de usuarios seran los siguientes:

**Admin**
* Agregar Universidades
* Ver analiticas de todos los cursos
* Hacer todo lo que los usuarios inferiores hacen.

**Manager**
* Decidir los roles de las personas
* Aprobar Los cursos

**Profesor**
* Curso
    + Crear un Curso
        - Agregar Modulos con sus Actividades
            * Peer 2 Peer
            * Verdadero y Falso
            * Selección Simple
    + Ver Estadisticas de Los Alumnos
        - % Completado de usuarios
        - Promedio de Nota
        - Cantidad de Gente Suscrita

**Alumno**
* Cursos
    + Ver Cursos Disponibles
    + Registrarse en los cursos
    + Cursos a los que esta suscrito
        - Participar en las Actividades de los modulos
        - Participar en El Foro
        - Dar una Reseña de Los Cursos
    + Historial de Cursos Realizados
        - % de Completacion
        - Otras Cosas
    + Certificado del Curso (Si se obtuvo)
* Perfil (Ver, Editar, Ver Perfil de otras personas, Ver Perfil de Profesores)
* Ver Otras cosas que pueda hacer el alumno

### Universidades:
Falta por desarrollar:
* Lista de Universidades
* Lista de cursos por universidad

### Cómo funciona:
Pagina sencilla para mostrar el codigo de honor y otras cosas