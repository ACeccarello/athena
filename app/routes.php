<?php
/**
 * Routes
 */

Route::get('/', [
     'as' => 'home',
     'uses' => function() {
        return View::make('hello');
    }
]);


Route::get('/iniciar-sesion', [
    'as' => 'login_path',
    'uses' => function() {
        return View::make('sessions.create');
    }
]);

Route::get('/registro', [
    'as' => 'register_path',
    'uses' => function () {
        return View::make('registration.create');
    }
]);


// grupo de cursos

Route::get('/cursos', [
    'as' => 'courses_path',
    'uses' => function()  {
        return View::make('courses.index');
    }
]);

Route::get('/cursos/{curso}', [
    'as' => 'course_path',
    'uses' => function($curso)  {
        return View::make('courses.course')->withCurso($curso);
    }
]);

// grupo de usuarios

Route::get('usuario', ['as' => 'user_path','uses' => 'UserProfileController@index']);
