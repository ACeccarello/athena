<?php

/*
 	User profile Controller.  Encargado de::

    /usuario/ << currentUser (logged in)
    /usuario/{id} << profile externo (not logged in) (if current id route to /)
    /usuario/editar << edit current profile (logged in)
    /usuario/certificados << currentUser (logged in)
    /usuario/historial <<  currentUser (logged in) (cursos realizados)
    /usuario/cursos << currentUser actualmente cursando (logged in)
    /usuario/{id}/eliminar << (manager/admin)

    /profesor/{profesorId} << profile Externo de Profesor
    /profesor/{profesorId}/cursos << cursos que da el profesor

    /{universidad}/ << Ver perfil de la universidad
    /{universidad}/edit << Editar perfil de la universidad (manager/admin)
    /{universidad}/cursos << cursos que ofrece la universidad
    /{universidad}/profesores << profesores de la universidad

 */

class UserProfileController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 * GET /userprofile
	 *
	 * @return Response
	 */
	public function index()
	{
		// filtro solo si hay usuario logeado
		// pasar el current user
		//
		// $user = [
		// 	"name" => "Alessandro Ceccarello",
		// 	"profilePic" => "images/placeholder.png",
		// ];
    	return View::make('users.index');
	}

	/**
	 * Display the specified resource.
	 * GET /userprofile/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 * GET /userprofile/{id}/edit
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 * PUT /userprofile/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 * DELETE /userprofile/{id}
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}