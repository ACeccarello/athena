<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title> @yield('title') </title>
    <!-- Global Stylesheets -->
    <link rel="stylesheet" href="{{URL::asset('vendor/open-sans-fontface/open-sans.css')}}">
    <link rel="stylesheet" href="{{URL::asset('vendor/bootstrap/dist/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{URL::asset('styles/main.css')}}">
    <!-- end of Global Stylesheets -->

    <!-- Global Scripts -->
    <!-- end of Global Scripts -->

</head>
<body>
    <header class="clearfix">
        <div class="container-fluid">
            <div class="col-md-3">
                <div class="brand">
                    <a href="{{ URL::route('home') }}">
                        <div class="logo">
                           <h1>Athena</h1>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="main-navigation">
                    <nav>
                        <ul class="list-inline">
                            <li><a href="{{URL::route('courses_path')}}">Cursos</a></li>
                            <li><a href="#">Como funciona?</a></li>
                            <li><a href="#">Universidades</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-md-3">
                <div class="user-menu">
                    <nav>
                        <ul class="list-inline">
                            <li><a href="{{URL::route('register_path')}}" class="link-button">Registro</a></li>
                            <li><a href="{{URL::route('login_path')}}" id="login-button">Iniciar Sesión</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    @yield('content')
    <footer class="clearfix">
    <div class="container">

        <div class="col-md-6">
            <div class="disclaimer">
                <p>Hola esto es un disclaimer Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam blanditiis commodi dicta dolor dolorem doloremque eaque eius eos esse eum harum incidunt ipsam iusto magni omnis, quae quibusdam quisquam repellat.</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="social-networks text-center">
                <ul class="list-inline" >
                    <li>Facebook</li>
                    <li>Twitter</li>
                    <li>Instagram</li>
                </ul>
            </div>
        </div>
        </div>
    </footer>
    </body>
     <!-- Global Scripts -->

    <!-- end of Global Scripts -->

    <!-- this page only Scripts -->
    @yield('scripts')
    <!-- end of this page only Scripts -->

</html>