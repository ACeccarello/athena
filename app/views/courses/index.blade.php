@extends('templates.main-layout')

@section('title') Cursos @stop

@section('content')

<div class="container-fluid">
    <!-- Left Sidebar -->
    <div class="courses-box clearfix">
        <div class="col-md-3">
            <div class="left-inner-addon ">
                <i class="fa fa-search"></i>
                <input type="search" class="form-control busqueda" placeholder="Busqueda" />
            </div>
            <h5>Categorías</h5>
            <div class="categorias">
                <div class="list-group">
                    <a href="#" class="list-group-item active">
                        Computación
                        <span class="badge">20</span>
                    </a>
                    <a href="#" class="list-group-item">
                        Historia
                        <span class="badge">14</span>
                    </a>
                    <a href="#" class="list-group-item">
                        Matematicas
                        <span class="badge">3</span>
                    </a>
                    <a href="#" class="list-group-item">
                        Biologia
                        <span class="badge">2</span>
                    </a>
                    <a href="#" class="list-group-item">
                        This
                        <span class="badge">6</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="course-list">
                <div class="course clearfix">
                    <div class="col-md-4">
                        <img src="{{ URL::asset('images/placeholder.png') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <h3>Titulo importante del curso</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vel rem cumque est, iste, doloribus officiis nobis cum blanditiis facere ratione quo eius! Numquam, soluta saepe quae nobis distinctio error.</p>
                        <a href="{{ URL::route('course_path', 'curso') }}" class="ver-mas pull-right">Ver Mas...</a>
                    </div>
                </div>
                <div class="course clearfix">
                    <div class="col-md-4">
                        <img src="{{ URL::asset('images/placeholder.png') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <h3>Titulo importante del curso</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vel rem cumque est, iste, doloribus officiis nobis cum blanditiis facere ratione quo eius! Numquam, soluta saepe quae nobis distinctio error.</p>
                        <a href="{{ URL::route('course_path', 'curso') }}" class="ver-mas pull-right">Ver Mas...</a>
                    </div>
                </div>
                <div class="course clearfix">
                    <div class="col-md-4">
                        <img src="{{ URL::asset('images/placeholder.png') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <h3>Titulo importante del curso</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vel rem cumque est, iste, doloribus officiis nobis cum blanditiis facere ratione quo eius! Numquam, soluta saepe quae nobis distinctio error.</p>
                        <a href="{{ URL::route('course_path', 'curso') }}" class="ver-mas pull-right">Ver Mas...</a>
                    </div>
                </div>
                <div class="course clearfix">
                    <div class="col-md-4">
                        <img src="{{ URL::asset('images/placeholder.png') }}" alt="">
                    </div>
                    <div class="col-md-8">
                        <h3>Titulo importante del curso</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic vel rem cumque est, iste, doloribus officiis nobis cum blanditiis facere ratione quo eius! Numquam, soluta saepe quae nobis distinctio error.</p>
                        <a href="{{ URL::route('course_path', 'curso') }}" class="ver-mas pull-right">Ver Mas...</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>


@stop