@extends('templates.main-layout')

@section('content')
    <div class="courses-box clearfix">

            <div class="container">
                <div class="col-md-8">
                    <div class="course-banner ">
                        <img src="{{ URL::asset('images/placeholder.png') }}" alt="">
                    </div>
                    <h2>Este es el titulo pro con 3 palabras mas y otro poco mas</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur provident earum qui ipsam laborum perferendis corporis in expedita ratione sequi doloremque eveniet, dignissimos perspiciatis voluptas cupiditate numquam porro, ab ut.</p>
                    <h3>Acerca de Este curso</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis inventore dolore aliquid laboriosam laborum ad perferendis at, amet, modi cupiditate perspiciatis illum vitae sunt, distinctio eveniet saepe doloremque soluta ducimus.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae aliquam ducimus optio, accusamus id quaerat commodi doloremque laudantium. Voluptates consequatur aspernatur explicabo dolor, iusto reiciendis illum ducimus aperiam, nam impedit.</p>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt tempora, voluptate iste saepe fugit aspernatur inventore consectetur eius sint voluptatem modi impedit commodi accusamus a, asperiores ea voluptatum distinctio totam.</p>
                    <p>loremLorem ipsum dolor sit amet, consectetur adipisicing elit. Possimus dolores tempore et nostrum consequatur itaque voluptatibus facilis aut quisquam dolorem, ex molestias, eaque tempora voluptate corporis incidunt temporibus fugit quasi.</p>
                    <h3>Prerequisitos</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et corporis consectetur id, perferendis aspernatur autem commodi, rerum doloremque! Sed aliquam atque officia autem nobis recusandae dicta, perspiciatis corporis consequuntur impedit.</p>

                    <h3>Acerca del Profesor del curso</h3>
                    <img src="{{ URL::asset('images/placeholder.png') }}" width="50px" height="50px" alt="">
                    <a href="#">Alessandro Ceccarello</a>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi vel, illo non quasi laboriosam harum minus accusamus tempora doloremque dolorum, repellendus aut dolore, at, quis iste officiis commodi cum molestias! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit temporibus, a? Cupiditate corporis, repudiandae fugiat voluptas atque culpa vel veniam saepe doloribus provident iure harum enim, modi debitis sunt! Maxime.</p>
                    <a href="#" class="subscribe-button">Subscribirse en este curso</a>
                </div>
                <div class="col-md-4">
                    <div class="sidebar">
                        <img src="{{ URL::asset('images/placeholder.png') }}" alt="">
                        <div class="course-description">
                            <div class="linea">
                                <span class="tag">Universidad</span>
                                <a href="#" class="pull-right">UJAP</a>
                            </div>
                            <div class="linea">
                                <span class="tag">Fecha de Inicio</span>
                                <span class="pull-right">21/12/1990</span>
                            </div>
                            <div class="linea">
                                <span class="tag">Duración del Curso</span>
                                <span class="pull-right">8 semanas</span>
                            </div>
                            <div class="linea">
                                <span class="tag">Profesor</span>
                                <a class="pull-right" href="#">Alessandro Ceccarello</a>
                            </div>
                        </div>
                        <div class="subscribe">
                            <a class="subscribe-button" href="#">Subscribirse al curso</a>
                        </div>
                        <div class="reviews">
                            Reseñas:
                            <div class="stars pull-right">
                                <span>42</span>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-star-o"></i></a>
                            </div>
                        </div>
                        <a href="#" class="review-button"><i class="fa fa-pencil"></i> Deja tus Comentarios del curso aqui!</a>
                        <div class="review-list">
                            <div class="review">
                                <div class="reviewer">
                                    <img src="{{ URL::asset('images/placeholder.png') }}" width="50px" height="50px" alt="">
                                    Por <a href="#">Alessandro Ceccarello con 30 m</a>
                                    <span class="rating text-right">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                </div>
                                <div class="review-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quaerat enim dolore omnis cupiditate laudantium <a href="#">... Ver mas</a>
                                </div>
                            </div>
                            <div class="review">
                                <div class="reviewer">
                                    <img src="{{ URL::asset('images/placeholder.png') }}" width="50px" height="50px" alt="">
                                    Por <a href="#">Alessandro C</a>
                                    <span class="rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                </div>
                                <div class="review-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quaerat enim dolore omnis cupiditate laudantium <a href="#">... Ver mas</a>
                                </div>
                            </div>
                            <div class="review">
                                <div class="reviewer">
                                    <img src="{{ URL::asset('images/placeholder.png') }}" width="50px" height="50px" alt="">
                                    Por <a href="#">Alessandro C</a>
                                    <span class="rating text-right">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o"></i>
                                    </span>
                                </div>
                                <div class="review-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium quaerat enim dolore omnis cupiditate laudantium <a href="#">... Ver mas</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop