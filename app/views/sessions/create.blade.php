@extends('templates.main-layout')

@section('title')
    Inicia Sesión
@stop

@section('content')
<div class="container">
    <div class="login-box clearfix">
        <div class="col-md-6">
            <div class="login-form clearfix">
                <p>Ingresa tu usuario y contraseña para entrar en el sitio</p>
                {{ Form::open( ['role' => 'form'] )}}
                    <div class="form-group">
                        <label for="email">Correo Electronico</label>
                        <input type="email" class="form-control" id="email" placeholder="ej: correo@ejemplo.com">
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input type="password" class="form-control" id="password" placeholder="Contraseña">
                    </div>
                    <a href="#" class="boton-link">Olvido su contraseña?</a>
                    <button type="submit" class=" boton-primario pull-right">Iniciar Sesión</button>
                {{ Form::close() }}
            </div>
        </div>
        <div class="col-md-6">
            <p>Si no estas registrado. Que esperas? ingresa tus datos en el siguiente enlace y tendras acceso a todos nuestros cursos:</p>
            <a href="{{URL::route('register_path')}}" class="boton-secundario">Registrarse</a>
        </div>
    </div>
</div>
@stop