@extends('templates.main-layout')

@section('title')Usuario -- {currentUser}@stop

@section('content')
<div class="profile-box clearfix">
    <div class="container">
        <div class="col-md-5">
            <div class="profile clearfix">
                <!-- Foto -->
                <div class="info">
                    <div class="foto">
                        {{ HTML::image('images/placeholder.png', 'Alessandro Ceccarello', ['height' => '75px', 'width' => '75px']) }}
                    </div>
                    <!-- Short Info -->
                    <div class="nombre">
                        <h4>Alessandro Ceccarello</h4>
                        <a href="#">UJAP</a>
                    </div>
                </div>
                <div class="info">
                    <div class="linea">
                        <span>Facebook:</span>
                        <a href="#">Facebook.com/woot</a>
                    </div>
                    <div class="linea">
                        <span>Twitter:</span>
                        <a href="#">Twitter.com/woot</a>
                    </div>
                    <div class="linea">
                        <span>Linkedin:</span>
                        <a href="#">Linkedin.com/woot</a>
                    </div>
                </div>

                <!-- Short Bio -->
                <div class="biografia">
                    <h5>Biografia</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis ipsam atque a hic obcaecati iure quis repellat excepturi molestias? Voluptatem magnam qui veniam, exercitationem cumque nobis repellendus nesciunt perferendis temporibus.</p>
                </div>
                <div class="edit">
                    <a class="pull-right" href="#"><i class="fa fa-edit"></i> Editar perfil</a>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <!-- Solo mostrar si estoy logeado -->

            <div class="profile clearfix">
                <!-- Menu (cursos, historial, certificados) -->
                <div class="menu">
                    <ul class="list-inline">
                        <li><a href="#">Cursos</a></li>
                        <li><a href="#">Historial</a></li>
                        <li><a href="#">Certificados</a></li>
                    </ul>
                </div>
                <div class="cursos-wrapper">
                    <div class="cursos">
                        <p>Cursos actualmente en progreso:</p>
                        <div class="curso clearfix">
                            <i class="fa fa-university"></i>
                            <div class="universidad">
                                <a href="#">UJAP</a>
                            </div>
                            <div class="fecha-fin">
                                Finaliza el: 21/12/2021
                            </div>
                            <div class="media">
                                <a class="media-left" href="#">
                                    {{ HTML::image('images/placeholder.png', 'Alessandro Ceccarello', ['height' => '75px', 'width' => '125px']) }}
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Titulo de un curso pro</h4>
                                    <p>Actualmente estas en el: Modulo 7 - Como hacer un mooc?</p>
                                     <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%;">
                                            <span>15%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="curso clearfix">
                            <i class="fa fa-university"></i>
                            <div class="universidad">
                                <a href="#">UJAP</a>
                            </div>
                            <div class="fecha-fin">
                                Finaliza el: 21/12/2021
                            </div>
                            <div class="media">
                                <a class="media-left" href="#">
                                    {{ HTML::image('images/placeholder.png', 'Alessandro Ceccarello', ['height' => '75px', 'width' => '125px']) }}
                                </a>
                                <div class="media-body">
                                    <h4 class="media-heading">Titulo de un curso pro</h4>
                                    <p>Actualmente estas en el: Modulo 7 - Como hacer un mooc?</p>
                                     <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%;">
                                            <span>15%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Solo mostrar si estoy logeado -->
        <!-- edit button -->
</div>
@stop
