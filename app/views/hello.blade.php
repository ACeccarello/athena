@extends('templates.main-layout')

@section('title') Athena @stop

@section('content')
    <div class="search-banner">
        <div class="search-bar">
            <div class="container-fluid">
                <div class="col-md-4 col-md-offset-1">
                    <p>¿Qué aprenderás hoy?</p>
                    <input type="search" class="form-control">
                </div>
            </div>
        </div>
    </div>
    <div class="small-triangle"></div>
    <div class="container">
        <div class="cursos-wrapper clearfix">
        <p class="starting-soon">Cursos que empiezan pronto:</p>
        <div class="cursos-box">
            <div class="col-md-3">
                <div class="curso">
                    <img src="{{ URL::asset('images/placeholder.png')}}" alt="">
                    <div class="content">
                        <span class="universidad">Universidad José Antonio Páez</span>
                        <div class="title">
                            <h3>Este es el titulo pro con 3 palabras mas y otro poco mas</h3>
                        </div>
                        <p>Empieza El: <span class="start-date">21/12/2014</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="curso">
                    <img src="{{ URL::asset('images/placeholder.png')}}" alt="">
                    <div class="content">
                        <span class="universidad">Universidad José Antonio Páez</span>
                        <div class="title">
                            <h3>Este es el titulo pro con 3 palabras mas y otro poco mas</h3>
                        </div>
                        <p>Empieza El: <span class="start-date">21/12/2014</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="curso">
                    <img src="{{ URL::asset('images/placeholder.png')}}" alt="">
                    <div class="content">
                        <span class="universidad">Universidad José Antonio Páez</span>
                        <div class="title">
                            <h3>Este es el titulo pro con 3 palabras mas y otro poco mas</h3>
                        </div>
                        <p>Empieza El: <span class="start-date">21/12/2014</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="curso">
                    <img src="{{ URL::asset('images/placeholder.png')}}" alt="">
                    <div class="content">
                        <span class="universidad">Universidad José Antonio Páez</span>
                        <div class="title">
                            <h3>Este es el titulo pro con 3 palabras mas y otro poco mas</h3>
                        </div>
                        <p>Empieza El: <span class="start-date">21/12/2014</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="curso">
                    <img src="{{ URL::asset('images/placeholder.png')}}" alt="">
                    <div class="content">
                        <span class="universidad">Universidad José Antonio Páez</span>
                        <div class="title">
                            <h3>Este es el titulo pro con 3 palabras mas y otro poco mas</h3>
                        </div>
                        <p>Empieza El: <span class="start-date">21/12/2014</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="curso">
                    <img src="{{ URL::asset('images/placeholder.png')}}" alt="">
                    <div class="content">
                        <span class="universidad">Universidad José Antonio Páez</span>
                        <div class="title">
                            <h3>Este es el titulo pro con 3 palabras mas y otro poco mas</h3>
                        </div>
                        <p>Empieza El: <span class="start-date">21/12/2014</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="curso">
                    <img src="{{ URL::asset('images/placeholder.png')}}" alt="">
                    <div class="content">
                        <span class="universidad">Universidad José Antonio Páez</span>
                        <div class="title">
                            <h3>Este es el titulo pro con 3 palabras mas y otro poco mas</h3>
                        </div>
                        <p>Empieza El: <span class="start-date">21/12/2014</span></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="curso">
                    <img src="{{ URL::asset('images/placeholder.png')}}" alt="">
                    <div class="content">
                        <span class="universidad">Universidad José Antonio Páez</span>
                        <div class="title">
                            <h3>Este es el titulo pro con 3 palabras mas y otro poco mas</h3>
                        </div>
                        <p>Empieza El: <span class="start-date">21/12/2014</span></p>
                    </div>
                </div>
            </div>
            <a href="#" class="ver-mas">Ver mas...</a>
        </div>
    </div>
    </div>
@stop