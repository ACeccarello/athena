@extends('templates.main-layout')

@section('title') Nuevo Usuario @stop

@section('content')
<div class="container">
    <div class="registration-box clearfix">
        <!-- Form  -->
        <div class="col-md-6">
            <p><span>Bienvenido!</span> Ingresa tus datos para crear tu cuenta en Athena!</p>
            <div class="register-form clearfix">
                {{ Form::open( ['role' => 'form'] )}}
                    <div class="form-group">
                        <label for="name">Nombre Completo</label>
                        <input type="text" class="form-control" id="name" placeholder="ej: Juan Doe">
                    </div>
                    <div class="form-group">
                        <label for="email">Correo Electronico</label>
                        <input type="email" class="form-control" id="email" placeholder="ej: correo@ejemplo.com">
                    </div>
                    <div class="form-group">
                        <label for="password">Contraseña</label>
                        <input type="password" class="form-control" id="password" placeholder="Contraseña">
                    </div>
                    <div class="form-group">
                        <label for="password_confirm">Repetir Contraseña</label>
                        <input type="password" class="form-control" id="password_confirm" placeholder="Contraseña">
                    </div>
                    <div class="form-group">
                        <label for="universidad">Universidad</label>
                        <select class="form-control" id="universidad">
                            <option value="1">Universidad Jose Antonio Paez</option>
                        </select>
                    </div>
                    <div class="terms">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Acepto el <a href="#">Código de Honor</a>
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"> Acepto los <a href="#">Términos y Condiciones</a>
                            </label>
                        </div>
                    </div>
                    <div class="register-button">
                        <button type="submit" class="boton-primario">Registrar</button>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
        <!-- end Form -->
        <!-- sidebar -->
        <div class="col-md-6">
            <h4>Registra tu cuenta en <span>Athena!</span></h4>
            <p>Al registrar tu cuenta en athena podras tener acceso a todos los cursos que tenemos en existencia y nuestros proximos cursos de manera gratuita.</p>
            <h4>Que hacer despues de registrarse?</h4>
            <p>Debes revisar en tu correo electronico el mensaje de confirmacion de cuenta y hacer click en el link de verificacion para terminar el proceso. No encuentras el correo? revisa en la carpeta de correo spam.</p>
            <h4>Ya tienes una cuenta?</h4>
            <p>Haz click en el siguiente enlace para acceder a tu cuenta: </p>
            <a href="{{URL::route('login_path')}}" class="boton-secundario">Iniciar Sesión</a>
        </div>
    </div>
</div>
@stop